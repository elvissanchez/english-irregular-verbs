# English irregular verbs

## Configuration
Create a virtual environment `python3 -m venv ./venv` and install the requirements `pip install --requirement requirements.txt`

## How to use?
You can see the **definition** or **translation** of the English irregular verb by typing `-d` or `-t` respectively.
